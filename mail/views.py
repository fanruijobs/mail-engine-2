from django.views import View
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login, logout

import os
import json
import smtplib
from email.mime.text import MIMEText
from email.header import Header

from .models import SenderEmailAddress, ReceiverEmailAddress, SingleMail, MailAttempt

# Create your views here.
@method_decorator(csrf_exempt, name='dispatch')
class SendMailView(View):
    def get(self, request, ):
        return HttpResponse("SendMailView")

    def post(self, request, ):
        reqBody = json.loads(request.body)

        username = reqBody['username']
        password = reqBody['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            mail_host= "smtp.gmail.com"  
            mail_user= reqBody["sender"]
            # mail_pass= os.environ["MAIL_PASSWORD"]   
            mail_receivers = reqBody["receivers"]
            mail_subject = "[" + username + "]-- " + reqBody["subject"]
            mail_message = reqBody["message"]

            email_address_senders = SenderEmailAddress.objects.filter(user=user, email_address=mail_user)
            if len(email_address_senders) == 0:
                return HttpResponse("This user does not have control of that email account. </h1>")
            email_address_sender = email_address_senders[0]

            for mail_receiver in mail_receivers:
                email_address_receiver, _ = ReceiverEmailAddress.objects.get_or_create(email_address=mail_receiver)
                send_report = sendMail(mail_host, email_address_sender.email_address, email_address_sender.email_password, [email_address_receiver.email_address], mail_subject, mail_message)
                
                single_mail = SingleMail(host=mail_host,
                                        sender=email_address_sender,
                                        receiver=email_address_receiver,
                                        subject=mail_subject,
                                        message=mail_message)
                single_mail.save()
                mail_attempt = MailAttempt(single_mail=single_mail)
                if send_report == '':  
                    mail_attempt.save()
                else:
                    mail_attempt.error_message = send_report
                    mail_attempt.is_success = False
                    mail_attempt.save()

            logout(request)
            return HttpResponse("Done.")
            
        else:
            return HttpResponse("Not Authenticated. ")


def sendMail(mail_host, mail_user, mail_pass, mail_receivers, mail_subject, mail_message):
    email_text = "\r\n".join([
        "From: "+ mail_user,
        "To: "+ ", ".join(mail_receivers),
        "Subject: "+ mail_subject,
        "",
        mail_message
    ])

    try:
        server = smtplib.SMTP_SSL(mail_host, 465)
        server.ehlo()
        server.login(mail_user, mail_pass)
        server.sendmail(mail_user, mail_receivers, email_text)
        server.close()
        print('Email sent!')
        return ''
    except Exception as e:
        print('Something went wrong...')
        print(e)
        return str(e)

