from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(SenderEmailAddress)
admin.site.register(ReceiverEmailAddress)
admin.site.register(SingleMail)
admin.site.register(MailAttempt)
