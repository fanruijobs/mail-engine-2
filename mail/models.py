from django.db import models
from django.contrib.auth.models import User


class SenderEmailAddress(models.Model):
    email_address = models.CharField(max_length=200)
    email_password = models.CharField(max_length=200)
    name = models.CharField(max_length=50, blank=True, null=True)

    user = models.ManyToManyField(User)

    def __str__(self):
        return self.email_address

class ReceiverEmailAddress(models.Model):
    email_address = models.CharField(max_length=200)
    name = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.email_address


class SingleMail(models.Model):
    host = models.CharField(max_length=50)
    sender = models.ForeignKey('SenderEmailAddress', on_delete=models.CASCADE, related_name='sender_email')
    receiver = models.ForeignKey('ReceiverEmailAddress', on_delete=models.CASCADE, related_name='receiver_email')
    subject = models.CharField(max_length=500)
    message = models.TextField()
    created_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.sender.email_address + ' to ' + self.receiver.email_address + ' on ' + str(self.created_time)

class MailAttempt(models.Model):
    single_mail = models.ForeignKey('SingleMail', on_delete=models.CASCADE)
    is_success = models.BooleanField(default=True)
    error_message = models.TextField(default='')    

    def __str__(self):
        return str(self.single_mail) + ', success: ' + str(self.is_success)


