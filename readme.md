# Email Engine
## Add User
Please add user for each app

## Add Sender Email Address
Use Google Email Service (GMail). You may need to activate 2-FA for the main login password, and open a "project/app" to use a non-2FA username-password pair.
Then you can add the username-password pair as Sender Email Address. 

## Then assign the Sender Email Address to User(App)

## Endpoints
### SendMail
~~~
POST /mail/sendMail/
~~~
With:
~~~
{
	"username": "xxx",
	"password": "xxx_password",
	"sender": "xxx@gmail.com",
	"receivers": ["xxxi@xxx.com"],
	"subject": "Test Subject",
	"message": "Test Message"
}
~~~
