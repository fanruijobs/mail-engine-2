# {
#     "dev": {
#         "aws_region": "ap-southeast-1",
#         "django_settings": "mail_engine.settings",
#         "project_name": "mail-engine",
#         "runtime": "python3.7",
#         "s3_bucket": "zappa-mail-engine-bucket",
#         "environment_variables": {
#             "DB_HOST": "personal-use.cepnuzpbj1cf.ap-southeast-1.rds.amazonaws.com",
#             "DB_PORT": "5432",
#             "DB_USER": "mail_engine_user",
#             "DB_PASSWORD": "mail_engine_password",
#             "DB_NAME": "mail_engine",
#             "MAIL_USER": "fan.rui.jobs@gmail.com",
#             "MAIL_PASSWORD": "hhnhgsvxpbhqmbaa"
#         }
#     }
# }

import json, os

# zappa_settings_json = json.load("../zappa_settings.json")

with open("zappa_settings_skeleton.json") as f:
    zappa_settings_json = json.load(f)

zappa_settings_json["dev"]["environment_variables"]["DB_HOST"] = os.environ["DB_HOST"]
zappa_settings_json["dev"]["environment_variables"]["DB_PORT"] = os.environ["DB_PORT"]
zappa_settings_json["dev"]["environment_variables"]["DB_USER"] = os.environ["DB_USER"]
zappa_settings_json["dev"]["environment_variables"]["DB_PASSWORD"] = os.environ["DB_PASSWORD"]
zappa_settings_json["dev"]["environment_variables"]["DB_NAME"] = os.environ["DB_NAME"]

with open("zappa_settings.json", "w") as f:
    json.dump(zappa_settings_json, f)
print(zappa_settings_json)